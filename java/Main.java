class Main {

    static long fib(int n) {
        if (n < 2) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public static void main(String[] args) {
        int N = 40;
        for (int i = 0; i <= N; i++) {
            System.out.println(fib(i));
        }
    }
}
