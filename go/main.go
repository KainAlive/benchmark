package main

import "fmt"

const N = 40

func fib(n int) int {
	if n < 2 {
		return n
	}
	return fib(n-1) + fib(n-2)
}

func main() {
	for i := 0; i <= N; i++ {
		fmt.Println(fib(i))
	}
}
