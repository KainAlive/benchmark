import std.stdio : writeln;

const int N = 40;

ulong fib(int n) {
    if (n < 2) {
        return n;
    }
    return fib(n-1) + fib(n-2);
}

void main() {
    for (int i = 0; i <= N; i++) {
        writeln(fib(i));
    }
}
