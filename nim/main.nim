const N: int = 40

proc fib(n: int): int =
  if n < 2:
    result = n
  else:
    result = fib(n - 1) + fib(n-2)

for i in 1..N:
  echo fib(i)
