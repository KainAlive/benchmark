#include <stdio.h>

#define N 40

unsigned long fib(int n) {
    if (n<2) {
        return n;
    }
    return fib(n-1) + fib(n-2);
}

int main() {
    for (int i = 0; i <= N; i++) {
        printf("%lu\n", fib(i));
    }
    return 0;
}
