N = 40


def fib(n: int) -> int:
    if n < 2:
        return n
    return fib(n - 1) + fib(n - 2)


for i in range(0, N + 1):
    print(fib(i))
