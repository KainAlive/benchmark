#!/bin/sh

echo "Cleaning up..."
rm ./bench
rm */main
rm */*.class
rm */*.o

echo ""
echo "Building..."
set -x
# Nim
nim c --opt:speed -d:release ./nim/main.nim
strip ./nim/main

# C
clang ./c/main.c -o ./c/main -Ofast
strip ./c/main

# Java
javac ./java/Main.java

#Go
cd ./go
go build ./main.go
cd ..

# D
ldc2 d/main.d -od=./d -of=./d/main --O3
strip ./d/main

# Zig
cd ./zig
zig build -Drelease-fast=true
cd ..

# Rust
rustc ./rust/main.rs -o ./rust/main

set +x

echo ""
echo "Running benchmarks..."

set -x

multitime -n 10 -q ./nim/main | tee ./bench
multitime -n 10 -q ./c/main | tee ./bench
multitime -n 10 -q python3 ./python/main.py | tee ./bench
multitime -n 10 -q pypy3 ./python/main.py | tee ./bench
cd ./java
multitime -n 10 -q java Main | tee ../bench 
cd ..
multitime -n 10 -q ./go/main | tee ./bench 
multitime -n 10 -q ./d/main | tee ./bench 
multitime -n 10 -q ./zig/zig-out/bin/main | tee ./bench
multitime -n 10 -q ./rust/main | tee ./bench

set +x

echo "Done:"
cat ./bench
