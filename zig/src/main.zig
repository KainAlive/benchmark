const std = @import("std");

fn fib(n: u32) u32 {
    if (n < 2) {
        return n;
    }
    return fib(n - 1) + fib(n - 2);
}

pub fn main() anyerror!void {
    const N: u8 = 40;
    var i: u8 = 0;
    const stdout = std.io.getStdOut().writer();
    while (i <= N) : (i += 1) {
        try stdout.print("{d}", .{fib(i)});
    }
}
