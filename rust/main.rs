fn fib(n: u32) -> u32 {
    if n<2 {
        return n;
    }
    return fib(n-1) + fib(n-2);
}

fn main() {
    
    let mut i = 0;
    while i <= 40 {
        println!("{}", fib(i));
        i+=1;
    }
}
